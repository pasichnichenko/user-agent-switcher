/*
 * User Agent Switcher
 * Copyright © 2017  Alexander Schlarb
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
"use strict";


// Connect to background page and request it to suspend processing of option changes
let port = browser.runtime.connect({name: "popup"});
port.postMessage({ request: "suspend-option-processing" });

// Resume option change processing when popup is backgrounded
document.addEventListener("visibilitychange", () => {
	if(document.hidden) {
		port.postMessage({ request: "resume-option-processing" });
	} else {
		port.postMessage({ request: "suspend-option-processing" });
	}
});

// Reload page if the list of available user-agents was updated
// (our entire UI is dynamically generated based on this information)
browser.storage.onChanged.addListener((changes, areaName) => {
	if(areaName !== "local") {
		return;
	}
	
	if(Object.keys(changes).includes("available")) {
		window.location.reload();
		return;
	}
});


function populateDocument(options, categories, categoryEntries) {
	let ignoreChangeEvents = false;
	
	/**
	 * "Open preferences" item
	 */
	document.getElementById("panel-item-preferences").addEventListener("click", (event) => {
		// Browser action popup was opened as a separate tab
		//XXX: Is there any way to detect this from context!?
		if(window.location.hash === "#tab") {
			window.location.href = browser.extension.getURL("content/options.html");
		//COMPAT: Firefox for Android 56-
		} else if(typeof(browser.runtime.openOptionsPage) !== "undefined") {
			browser.runtime.openOptionsPage();
			window.close();
		} else {
			browser.tabs.create({
				active: true,
				url:    browser.extension.getURL("content/options.html")
			}).then(window.close, console.exception);
		}
	});
	
	
	/**
	 * "Random User-Agent" mode configuration
	 */
	/* Global random-mode toggle */
	let DOMRandomCheckbox   = document.getElementById("panel-item-random");
	let DOMRandomSubsection = document.getElementById("random-subsection");
	function updateRandomState(randomEnabled) {
		DOMRandomCheckbox.checked = randomEnabled;
	}
	function applyRandomState(event) {
		// Show or hide random configuration subsection based on state of the global checkbox
		DOMRandomSubsection.style.display = DOMRandomCheckbox.checked ? "block" : "none";
		
		// Also update storage if this function was called because of an user-initiated event
		if(event && !ignoreChangeEvents) {
			browser.storage.local.set({
				"random-enabled": DOMRandomCheckbox.checked
			}).catch(console.exception);
		}
	}
	DOMRandomCheckbox.addEventListener("change", applyRandomState);
	updateRandomState(options["random-enabled"]);
	applyRandomState();
	
	
	/* Random-mode interval */
	let DOMRandomIntervalStartup = document.getElementById("random-interval-mode-startup");
	let DOMRandomIntervalTimed   = document.getElementById("random-interval-mode-timed");
	let DOMRandomIntervalTimeValue = document.getElementById("random-interval-time-value");
	let DOMRandomIntervalTimeUnit  = document.getElementById("random-interval-time-unit");
	function updateRandomIntervalState(randomInterval) {
		switch(randomInterval.mode) {
			case "startup":
				DOMRandomIntervalStartup.checked = true;
				break;
			case "timed":
				DOMRandomIntervalTimed.checked = true;
				break;
		}
		DOMRandomIntervalTimeValue.value = randomInterval.value;
		DOMRandomIntervalTimeUnit.value  = randomInterval.unit;
	}
	function applyRandomIntervalState(event) {
		// Only enable the time value and unit fields if their radio button is selected
		DOMRandomIntervalTimeValue.disabled = !DOMRandomIntervalTimed.checked;
		DOMRandomIntervalTimeUnit.disabled  = !DOMRandomIntervalTimed.checked;
		
		// Also update storage if this function was called because of an user-initiated event
		if(event && !ignoreChangeEvents) {
			let mode;
			if(DOMRandomIntervalStartup.checked) {
				mode = "startup";
			} else if(DOMRandomIntervalTimed.checked) {
				mode = "timed";
			} else {
				return;
			}
			
			browser.storage.local.set({
				"random-interval": {
					mode:  mode,
					value: DOMRandomIntervalTimeValue.value,
					unit:  DOMRandomIntervalTimeUnit.value,
				}
			}).catch(console.exception);
		}
	}
	DOMRandomIntervalStartup.addEventListener("change", applyRandomIntervalState);
	DOMRandomIntervalTimed.addEventListener("change", applyRandomIntervalState);
	DOMRandomIntervalTimeValue.addEventListener("change", applyRandomIntervalState);
	DOMRandomIntervalTimeUnit.addEventListener("change", applyRandomIntervalState);
	updateRandomIntervalState(options["random-interval"]);
	applyRandomIntervalState();
	
	// When selecting the interval-based random-mode timer, auto-focus the time-value field
	DOMRandomIntervalTimed.addEventListener("click", (event) => {
		DOMRandomIntervalTimeValue.focus();
	});
	
	
	/* Random-mode categories */
	let DOMRandomCategories = document.getElementById("random-subsection");
	function populateRandomCategories(randomCategories) {
		for(let i = 0; i < categories.length; i++) {
			let [categoryOrig, category] = categories[i];
			
			let DOMRandomCategoryItem = document.createElement("div");
			DOMRandomCategoryItem.className = "panel-list-item browser-style";
			
			let DOMRandomCategoryCheckbox = document.createElement("input");
			DOMRandomCategoryCheckbox.type    = "checkbox";
			DOMRandomCategoryCheckbox.name    = "random-categories";
			DOMRandomCategoryCheckbox.id      = `random_category_${i}`;
			DOMRandomCategoryCheckbox.value   = categoryOrig;
			DOMRandomCategoryCheckbox.checked = randomCategories.includes(categoryOrig);
			DOMRandomCategoryItem.appendChild(DOMRandomCategoryCheckbox);
			
			let DOMRandomCategoryLabel = document.createElement("label");
			DOMRandomCategoryLabel.className   = "text";
			DOMRandomCategoryLabel.htmlFor     = `random_category_${i}`;
			DOMRandomCategoryLabel.textContent = category;
			DOMRandomCategoryItem.appendChild(DOMRandomCategoryLabel);
			
			DOMRandomCategories.appendChild(DOMRandomCategoryItem);
			
			DOMRandomCategoryCheckbox.addEventListener("change", applyRandomCategoriesState);
		}
	}
	function updateRandomCategoriesState(randomCategories) {
		for(let DOMRandomCategoryCheckbox of document.getElementsByName("random-categories")) {
			DOMRandomCategoryCheckbox.checked =
					randomCategories.includes(DOMRandomCategoryCheckbox.value);
		}
	}
	function applyRandomCategoriesState(event) {
		if(event && !ignoreChangeEvents) {
			let randomCategories = [];
			for(let DOMRandomCategoryCheckbox of document.getElementsByName("random-categories")) {
				if(DOMRandomCategoryCheckbox.checked) {
					randomCategories.push(DOMRandomCategoryCheckbox.value);
				}
			}
			
			browser.storage.local.set({
				"random-categories": randomCategories
			}).catch(console.exception);
		}
	}
	populateRandomCategories(options["random-categories"]);
	applyRandomCategoriesState();
	
	
	/**
	 * Static User-Agent selector
	 */
	document.getElementById("ua_default").addEventListener("change", (event) => {
		if(ignoreChangeEvents) {
			return;
		}
		
		// Special "Default" item selected
		browser.storage.local.set({
			"current": null,
			
			// Disable random selection mode as well
			"random-enabled": false
		}).then(window.close, console.exception);
	});
	
	let DOMAgentList = document.getElementById("agent-list");
	function populateUserAgentList(current, collapsedCategories) {
		let counter = 1;
		for(let [categoryOrig, category] of categories) {
			let DOMCategoryHeader = document.createElement("div");
			DOMCategoryHeader.className = "panel-section-header subsection collapsible category-header";
			DOMCategoryHeader.dataset["name"] = categoryOrig;
			
			let DOMCategoryHeaderText = document.createElement("div");
			DOMCategoryHeaderText.className   = "text-section-header";
			DOMCategoryHeaderText.textContent = category;
			DOMCategoryHeader.appendChild(DOMCategoryHeaderText);
			
			DOMAgentList.appendChild(DOMCategoryHeader);
			
			let DOMCategoryBody = document.createElement("div");
			DOMCategoryBody.className = "collapsible-body";
			
			for(let { label, string } of categoryEntries[category]) {
				let DOMAgentItem = document.createElement("div");
				DOMAgentItem.className = "panel-list-item browser-style";
				DOMAgentItem.title     = string;
				
				let DOMAgentRadio = document.createElement("input");
				DOMAgentRadio.type    = "radio";
				DOMAgentRadio.name    = "current";
				DOMAgentRadio.id      = `ua_${counter}`;
				DOMAgentRadio.checked = (string === current);
				DOMAgentRadio.dataset["string"] = string;
				DOMAgentItem.appendChild(DOMAgentRadio);
				
				let DOMAgentLabel = document.createElement("label");
				DOMAgentLabel.className   = "text";
				DOMAgentLabel.htmlFor     = `ua_${counter}`;
				DOMAgentLabel.textContent = label;
				DOMAgentItem.appendChild(DOMAgentLabel);
				
				DOMCategoryBody.appendChild(DOMAgentItem);
				
				DOMAgentRadio.addEventListener("change", applyUserAgentSelection);
				
				counter++;
			}
			
			DOMAgentList.appendChild(DOMCategoryBody);
			
			// Store effective height of collapsible element body to work around
			// "height: auto" CSS transition issues
			DOMCategoryBody.style.height = `${DOMCategoryBody.clientHeight}px`;
			
			// Now apply the collapsed stated – after contents have been measured
			if(collapsedCategories.includes(categoryOrig)) {
				DOMCategoryBody.style.transition = "none";
				DOMCategoryHeader.classList.add("collapsed");
				DOMCategoryBody.style.transition = null;
			}
			
			/* Add event handler for collapsing and uncollapsing sections */
			DOMCategoryHeader.addEventListener("click", (event) => {
				if(DOMCategoryHeader.classList.contains("collapsed")) {
					//WORKAROUND: Prevent scrollbar from appearing while uncollapsing element
					if(document.documentElement.scrollHeight === document.documentElement.clientHeight) {
						document.documentElement.style.overflowY = "hidden";
						window.setTimeout(() => {
							document.documentElement.style.overflowY = null;
						}, 600);
					}
					
					DOMCategoryHeader.classList.remove("collapsed");
				} else {
					DOMCategoryHeader.classList.add("collapsed");
				}
				
				applyUserAgentCollapsedCategories(event);
			});
		}
	}
	function updateUserAgentCollapsedCategories(collapsedCategories) {
		for(let DOMCategoryHeader of document.getElementsByClassName("category-header")) {
			if(collapsedCategories.includes(DOMCategoryHeader.dataset["name"])) {
				DOMCategoryHeader.classList.add("collapsed");
			} else {
				DOMCategoryHeader.classList.remove("collapsed");
			}
		}
	}
	function applyUserAgentCollapsedCategories(event) {
		console.log(event, ignoreChangeEvents);
		if(event && !ignoreChangeEvents) {
			// Update list of collapsed categories
			let collapsedCategories = [];
			for(let DOMCategoryHeader of document.getElementsByClassName("category-header")) {
				if(DOMCategoryHeader.classList.contains("collapsed")) {
					collapsedCategories.push(DOMCategoryHeader.dataset["name"]);
				}
			}
			console.log(collapsedCategories);
			browser.storage.local.set({
				"popup-collapsed": collapsedCategories
			}).catch(console.exception);
		}
	}
	function updateUserAgentSelection(current) {
		for(let DOMAgentRadio of document.getElementsByName("current")) {
			if(DOMAgentRadio.dataset["string"] === current) {
				DOMAgentRadio.checked = true;
			}
		}
	}
	function applyUserAgentSelection(event) {
		if(event && !ignoreChangeEvents) {
			// Radio button with User-Agent selected (possibly through its label)
			browser.storage.local.set({
				"current": event.target.dataset["string"],
				
				// Disable random selection mode if manual selection has been performed
				"random-enabled": false
			}).then(window.close, console.exception);
		}
	}
	populateUserAgentList(options["current"], options["popup-collapsed"]);
	applyUserAgentSelection();
	applyUserAgentCollapsedCategories();
	
	
	/**
	 * Update page on configuration changes
	 */
	browser.storage.onChanged.addListener((changes, areaName) => {
		if(areaName !== "local") {
			return;
		}
		
		ignoreChangeEvents = false;
		try {
			for(let name of Object.keys(changes)) {
				switch(name) {
					case "current":
						updateUserAgentSelection(changes[name].newValue);
						applyUserAgentSelection();
						break;
					
					case "random-enabled":
						updateRandomState(changes[name].newValue);
						applyRandomState();
						break;
					
					case "random-categories":
						updateRandomCategoriesState(changes[name].newValue);
						applyRandomCategoriesState();
						break;
					
					case "random-interval":
						updateRandomIntervalState(changes[name].newValue);
						applyRandomIntervalState();
						break;
					
					case "popup-collapsed":
						updateUserAgentCollapsedCategories(changes[name].newValue);
						applyUserAgentCollapsedCategories();
						break;
				}
			}
		} finally {
			ignoreChangeEvents = false;
		}
	});
}


function waitForDocumentReady() {
	return new Promise((resolve, reject) => {
		window.addEventListener("DOMContentLoaded", () => {
			resolve();
		});
	});
}

Promise.all([waitForDocumentReady(), browser.storage.local.get()]).then(([_, options]) => {
	let entries = options["available"];
	
	// Separate the stored User-Agent entries into sections
	let categoryEngText2locale = TextEntryCategories.getEngText2Locale();
	let categories      = [];
	let categoryEntries = {};
	for(let entry of entries) {
		if(entry.type !== "user-agent") {
			continue;
		}
		
		let category = entry.category;
		
		// Localize default category names
		if(categoryEngText2locale.hasOwnProperty(category)) {
			category = categoryEngText2locale[category];
		}
		
		// Remember new category names with their order
		if(!categoryEntries.hasOwnProperty(category)) {
			categoryEntries[category] = [];
			categories.push([entry.category, category]);
		}
		
		// Add entry to its category
		categoryEntries[category].push(entry);
	}
	
	populateDocument(options, categories, categoryEntries);
});
