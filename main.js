/*
 * User Agent Switcher
 * Copyright © 2017  Alexander Schlarb
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
//"use strict";

/**
 * Default values for all options
 *
 * Defined here so that they are instantly available until the actual value can be loaded from
 * storage.
 */
const OPTIONS_DEFAULT = {
	"current": null,
	
	"available":         null,
	"available-changed": false,
	
	"random-enabled":    false,
	"random-categories": [TextEntryCategories.DESKTOP_ENGTEXT],
	"random-interval":   {mode: "startup", value: 1, unit: "h"},
	
	"popup-collapsed": [],
	
	"edit-mode": "table"
};


/**
 * Track current add-on options, so that their are always available when resolving a request
 */
// Start with the default options
let options = Object.assign({}, OPTIONS_DEFAULT);
let optionsLockCount   = 0;
let optionsLockChanges = {};

Promise.resolve().then(() => {
	// Load all currently set options from storage
	return browser.storage.local.get();
}).then((result) => {
	// Update the default options with the real ones loaded from storage
	Object.assign(options, result);
	
	// Read default list of available user agents from file if none was found in storage or the user
	// has never edited it (so they will automatically stay up-to-date unless they change something)
	if(!options["available"] || !options["available-changed"]) {
		return fetch(browser.extension.getURL("assets/user-agents.txt"))
				.then((response) => response.text())
				.then((content)  => {
					let parser = new TextEntryParser();
					options["available"] = parser.parse(content);
				});
	}
}).then(() => {
	//Migration-1.4: Add the default category label of "Other" for entries that lack one
	for(let entry of options.available) {
		if(entry.type === "user-agent" && typeof(entry.category) !== "string") {
			entry.category = TextEntryCategories.OTHER_ENGTEXT;
		}
	}
	
	// Write back the final option list so that the defaults are properly displayed on the
	// options page as well
	return browser.storage.local.set(options);
}).then(() => {
	// Possibly enable the request listener already
	return applyRandomMode().then(() => {
		applyUserAgentConfiguration();
		updateBrowserAction();
	});
}).then(() => {
	// Keep track of new developments in option land
	browser.storage.onChanged.addListener((changes, areaName) => {
		if(areaName !== "local") {
			return;
		}
		
		if(optionsLockCount < 1) {
			// Update state based on which option keys actually changed
			applyOptionChanges(changes);
		} else {
			// Remember list of changed options for later
			for(let name of Object.keys(changes)) {
				optionsLockChanges[name] = changes[name];
			}
		}
	});
	
	// Done setting up options
}).catch(console.exception);


function applyOptionChanges(changes) {
	let changedOptionKeys = [];
	
	// Apply changes to option storage while recording which keys *actually* have a new value
	for(let name of Object.keys(changes)) {
		if(JSON.stringify(changes[name].newValue) !== JSON.stringify(options[name])) {
			changedOptionKeys.push(name);
		}
		
		options[name] = changes[name].newValue;
	}
	
	let changesApplied = false;
	
	// Call the respective update functions as necessary
	for(let name of changedOptionKeys) {
		switch(name) {
			case "current":
				applyUserAgentConfiguration();
				changesApplied = true;
				break;
				
			case "random-enabled":
			case "random-categories":
			case "random-interval":
				applyRandomMode().catch(console.exception);
				changesApplied = true;
				break;
		}
	}
	
	// Update browser action icon if any changes where actually applied
	if(changesApplied) {
		updateBrowserAction();
	}
}


/***************/
/* HTTP Header */
/***************/

/**
 * Callback function for processing about-to-be-sent blocking requests and
 * modifying their "User-Agent"-header based on the current options
 */
function requestListener(request) {
	if(typeof(options["current"]) === "string") {
		for(var header of request.requestHeaders) {
			if(header.name.toLowerCase() === "user-agent") {
				header.value = options["current"];
			}
		}
	}
	
	return {
		requestHeaders: request.requestHeaders
	};
}



/**************/
/* JavaScript */
/**************/

/**
 * Callback function for modifying a tab's `navigator.userAgent` object based
 * on the current options
 */
function tabListener(tabId, changeInfo, tab) {
	if(typeof(options["current"]) === "string" && changeInfo["status"] === "loading") {
		browser.tabs.executeScript(tabId, {
			"allFrames": true,
			"runAt":     "document_start",
			"matchAboutBlank": true,
			
			"code": `
			let userAgent = decodeURIComponent("${encodeURIComponent(options["current"])}");
			function overrideUserAgent() {
				console.log("Overriding User-Agent", window.location.href);
				Object.defineProperty(window.navigator.wrappedJSObject, "userAgent", {
					enumerable: true,
					value:      userAgent
				});
			};
			void(overrideUserAgent())`
		}).then((result) => {
			console.log("Tab listener result:", result);
		})
	}
}


/********************************
 * Orchestration and GUI tweaks *
 ********************************/
let randomModeTimer = null;
function applyRandomMode() {
	// Cancel any previous timer for running this function
	if(randomModeTimer !== null) {
		window.clearTimeout(randomModeTimer);
		randomModeTimer = null;
	}
	
	// Nothing else to do if random mode isn't actually enabled
	if(!options["random-enabled"]) {
		return Promise.resolve();
	}
	
	// Build list of applicable User-Agent-entries
	let entries = [];
	for(let entry of options["available"]) {
		if(entry.type == "user-agent" && options["random-categories"].includes(entry.category)) {
			entries.push(entry);
		}
	}
	
	// Select entry using a poor, but good enough, random number generator
	let entry = entries[Math.floor(Math.random() * entries.length)];
	
	// Set new user-agent string
	return browser.storage.local.set({
		"current": entry.string
	}).then(() => {
		if(options["random-interval"].mode === "timed") {
			let millis;
			switch(options["random-interval"].unit) {
				case "m":
					millis = options["random-interval"].value * 60 * 1000;
					break;
				case "h":
					millis = options["random-interval"].value * 3600 * 1000;
					break;
				case "d":
					millis = options["random-interval"].value * 86400 * 1000;
					break;
				default:
					return;
			}
			
			randomModeTimer = window.setTimeout(applyRandomMode, millis);
		}
	});
}


function generateIconBadgeText(userAgent) {
	// No text if disabled
	if(typeof(userAgent) !== "string") {
		return "";
	}
	
	// Lookup human-readable label for current UA string
	let entryLabel = null;
	for(let entry of options["available"]) {
		if(entry.type === "user-agent" && entry.string === userAgent) {
			entryLabel = entry.label;
			break;
		}
	}
	
	if(typeof(entryLabel) !== "string") {
		return "";
	}
	
	// Vary based on label style
	if(entryLabel.includes("/")) {
		// Style used by the default list: <OS> / <Browser> => O/B
		return entryLabel.split("/", 2).map((s) => s.trim().substr(0,1)).join("/").toUpperCase();
	} else if(entryLabel.includes(" ")) {
		// More than one word: <One> <Two> <Three> => OTT
		return entryLabel.split(/\s+/g, 3).map((s) => s.substr(0,1)).join("").toUpperCase();
	} else {
		// Just one word: <Word> => WO
		return entryLabel.substr(0, 2).toUpperCase();
	}
}

function generateIconTitle(userAgent, randomEnabled) {
	let titleMsgID = "icon_title_"
		+ (randomEnabled ? "random" : (typeof(userAgent) === "string" ? "enabled" : "disabled"));
	
	let title = browser.runtime.getManifest().name + " – " + browser.i18n.getMessage(titleMsgID);
	if(typeof(userAgent) === "string") {
		title += " (" + generateIconBadgeText(userAgent) + ")";
	}
	return title;
}

/**
 * Update the extension icon, title & badge based on the current settings
 */
function updateBrowserAction() {
	// Update text
	browser.browserAction.setTitle({
		title: generateIconTitle(options["current"], options["random-enabled"])
	});
	
	// Update icon
	if(options["random-enabled"]) {
		// Colored icon for random-mode
		//COMPAT: Firefox for Android 55+
		if(typeof(browser.browserAction.setIcon) !== "undefined") {
			browser.browserAction.setIcon({ path: "assets/icon-random.svg" });
		}
	} else if(typeof(options["current"]) === "string") {
		// Colored icon
		//COMPAT: Firefox for Android 55+
		if(typeof(browser.browserAction.setIcon) !== "undefined") {
			browser.browserAction.setIcon({ path: "assets/icon.svg" });
		}
	} else {
		// Grayscale icon, "disabled" text
		//COMPAT: Firefox for Android 55+
		if(typeof(browser.browserAction.setIcon) !== "undefined") {
			browser.browserAction.setIcon({ path: "assets/icon-disabled.svg" });
		}
	}
	
	// Update badge
	//COMPAT: Firefox for Android 55+
	if(typeof(browser.browserAction.setBadgeText)            !== "undefined"
	&& typeof(browser.browserAction.setBadgeBackgroundColor) !== "undefined") {
		let badgeColor = options["random-enabled"] ? "darkgreen" : "darkgray";
		browser.browserAction.setBadgeText({ text: generateIconBadgeText(options["current"]) });
		browser.browserAction.setBadgeBackgroundColor({ color: badgeColor });
	}
}

/**
 * Start or stop the HTTP header and JavaScript modifications
 */
let processingEnabled = false;
function applyUserAgentConfiguration() {
	if(!processingEnabled && typeof(options["current"]) === "string") {
		processingEnabled = true;
		browser.webRequest.onBeforeSendHeaders.addListener(
			requestListener,
			{urls: ["<all_urls>"]},
			["blocking", "requestHeaders"]
		);
		browser.tabs.onUpdated.addListener(tabListener);
	} else if(processingEnabled && typeof(options["current"]) !== "string") {
		browser.tabs.onUpdated.removeListener(tabListener);
		browser.webRequest.onBeforeSendHeaders.removeListener(requestListener);
		processingEnabled = false;
	}
}

/**********************
 * Content script API *
 **********************/

browser.runtime.onConnect.addListener((port) => {
	let suspendsOptionProcessing = false;
	function resumeOptionProcessing() {
		if(suspendsOptionProcessing) {
			port.onDisconnect.removeListener(resumeOptionProcessing);
			suspendsOptionProcessing = false;
			
			optionsLockCount--;
			if(optionsLockCount < 1) {
				applyOptionChanges(optionsLockChanges);
				optionsLockChanges = {};
			}
		}
	}
	
	port.onMessage.addListener((message) => {
		switch(message.request) {
			// Client request to stop processing any storage updates
			// until it either sends "unblock-storage-updates" or
			// disconnects
			case "suspend-option-processing":
				if(!suspendsOptionProcessing) {
					suspendsOptionProcessing = true;
					port.onDisconnect.addListener(resumeOptionProcessing);
					
					optionsLockCount++;
				}
				break;
			case "resume-option-processing":
				resumeOptionProcessing();
				break;
		}
	});
});


/*************************************************
 * Browser action workarounds for Firefox Mobile *
 *************************************************/
// Open popup in new tab, if `browser.browserAction.setPopup` is not supported
//COMPAT: Firefox for Android 55+
if(typeof(browser.browserAction.setPopup) === "undefined") {
	let popupURL = browser.extension.getURL("content/popup.html#tab");
	browser.browserAction.onClicked.addListener((tab) => {
		browser.tabs.create({
			active: true,
			url:    popupURL
		}).catch(console.exception);
	});
}